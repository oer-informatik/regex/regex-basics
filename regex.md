## Mustererkennung mit Regular Expressions (RegEx)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/regex</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110397061085890869</span>


> **tl/dr;** _(ca. 20 min Lesezeit): Die Mustererkennung Regulärer Ausdrücke (RegEx) ermöglicht Automatisierungen an Stellen, wo Handarbeit mühsam und langwierig ist. Telefonnummern in unterschiedlichen Formaten gespeichert? In einer CSV-Datei alle Spalten in verkehrter Reihenfolge? Wo stecken im Fließtext Geldbeträge mit Nachkommastellen? Derlei Aufgaben lassen sich mit RegEx lösen - besser man kennt sie!_

### Was sind _Reguläre Ausdrücke_?

Ein _Regulärer Ausdruck_ (RegEx) ist eine Zeichenkette, die Muster in Zeichenketten mithilfe von syntaktischen Regeln beschreibt.

_Reguläre Ausdrücke_ helfen vor allem dabei, Teilstrings zu finden oder zu ersetzen, die einem einheitlichen Muster entsprechen, sich aber nicht durch einfache Wildcards (wie z.B. ? oder *) ausdrücken lassen.

So lassen sich beispielsweise relativ einfach Muster bilden, die in einem vorhandenen langen Text alle Geldbeträge, Telefonnummern, Internetlinks, EAN-Nummern oder E-Mail-Adressen finden.

![Mit RegEx wird man zum Superhelden! (von xkcd, CC BY-NC 2.5 [https://xkcd.com/208/](https://xkcd.com/208/))](https://imgs.xkcd.com/comics/regular_expressions.png)

Ein Einstiegs-Beispiel für eine RegEx: die Klassenbezeichnungen an unserer Schule folgen der Logik:

```
1. Großbuchstabe: Abteilung
2.+3. Großbuchstabe: Ausbildungsberuf
4. Zahl: Einschulungsjahr (letzte Stelle)
5. Großbuchstaben für Parallelklassen
==> 3 Großbuchstaben, eine Zahl, ein Großbuchstabe
```

Ein _Regulärer Ausdruck_, der das beschreiben würde, wäre beispielsweise:

```regex
[A-Z][A-Z][A-Z][0-9][A-Z]
```

Es wurden einfach fünf Zeichenklassen hintereinander geschrieben. Das geht noch einfacher:

```regex
[A-Z]{3}[0-9][A-Z]
```

- `[A-Z]` definiert hier die _Zeichenklasse_ der Großbuchstaben (alle Zeichen, die in der ASCII/Unicode-Tabelle zwischen `A` und `Z` stehen)

- `[0-9]` die _Zeichenklasse_ der Zahlen (ASCII zwischen 0 und 9)

- `{3}` definiert das mehrfache Vorkommen der Zeichenklasse (ein _Quantifizierer_).

Suchmuster bestehen also in erster Linie aus Aneinanderreihungen von Zeichenklassen und Quantifizierern. 

### Grundlagen: Einfache RegEx

Die beiden Grundlagen habe wir ja oben bereits kennengelernt: Zeichenklassen und Quantifizierer. Schauen wir uns diese Elemente mal etwas genauer an:

#### Definition von Zeichenklassen

Die einfachste Art, Zeichenklassen zu definieren, ist es, alle Zeichen, die man finden will, in eckige Klammern zu schreiben. Die Inhalte in der eckigen Klammer sind ODER-verknüpft.

|RegEx|Erläuterung|
|---|---|
|`[aeiou]`,<br/>`[a-z]`,`[A-Z]`,<br/>`[0-9]`, <br/>`[a-zA-Z0-9]`|Zeichenklassen passen auf ein Zeichen, das in der Klasse enthalten ist.<br> `[aeiou]` passt auf a, e, i, o oder u, aber nicht auf x. Es können auch Bereiche angegeben werden,<br/> `[a-z]` passt auf alle Kleinbuchstaben. <br/>`[a-zA-Z0-9]` Bereiche können auch kombiniert werden.<br/> Die Sonderzeichen verlieren in einer Klasse ihre Wirkung: Punkt, Stern, Fragezeichen (siehe oben: Metacharaktere)|
|`[^abc]`, <br/>`[^a-z]`,<br/>`[^a-zA-Z0-9]`|`^` negiert die Zeichenklassen:<br/> `[^abc]` passt auf jedes beliebige Zeichen, außer a, b und c.<br/> (!! außerhalb der `[ ]` steht `^` für Eingabebeginn!! s.o.)|
|`[a-z&&[^mn]]`|Verschachtelung und Ausschluss: a bis z außer m und n|

#### Quantifizierer: Häufigkeit des Auftretens (außerhalb der Zeichenklassendefinition!)

|RegEx|Erläuterung|
|---|---|
|`*`|Das vorangehende Element (ein einzelnes Zeichen, eine geklammerte Zeichenfolge oder eine Zeichenklasse) <br/>darf **beliebig oft** vorkommen, auch keinmal.|
|`+`|Das vorangehende Element <br/>muss **mindestens einmal**, darf aber auch öfter vorkommen.|
|`?`|Das vorangehende Element darf <br/> **nicht oder genau einmal** vorkommen, aber keinesfalls öfter.|
|`{n}`|Das vorangehende Element <br/>muss **genau n-mal** vorkommen.|
|`{n,}`|Das vorangehende Element <br/>muss **mindestens n-mal** vorkommen.|
|`{n, m}`|Das vorangehende Element <br/>muss **mindestens n-mal aber höchstens m-mal** vorkommen.|
|`{,m}`|Das vorangehende Element <br/>darf **höchstens m-mal vorkommen**. (geht bei wenigen Parsern)|

#### Lazy / Greedy

Reguläre Ausdrücke mit den Quantifizierern `*` und `+` sind gierig (_greedy_): sie versuchen, so viel wie möglich Treffer zu erzielen und das Suchmuster über möglichst viele Zeichen auszudehnen. In der Regel ist dies jedoch nicht gewünscht: Bestimmte Zeichenklassen sollen sich nur so lange wiederholen, bis das erste Vorkommen der folgenden Zeichenklasse auftritt. Diese genügsamere Suchcharakteristik (_lazy_) lässt sich umstellen, in dem die _lazy_ Varianten der Multiplizität genutzt werden (`*?` und `+?`): Das Fragezeichen kommt hier als Metacharakter erneut zum Zug:

|Beispieltext|_greedy_ RegEx|_lazy_ RegEx|
|---|---|---|
|`<kunden><kunde></kunde></kunden>`|`<.*>`<br/>findet Gesamttext|`<.*?>` <br/> findet nur `<kunden>`| 
|`Es gibt kein richtiges Leben im falschen`|`E.+n` <br/> findet Gesamttext|`E.+?n` <br/> findet `Es gibt kein`|
|`dadada`|`(da){1,}` <br/> findet dadada|`(da){1,}?`<br/> findet drei mal da|

### Literale und Metacharaktere als Elementarbausteine

In _Regulären Ausdrücken_ wird zwischen Literalen und Metacharakteren unterschieden: 
Literale repräsentieren das jeweilige Zeichen, Metacharaktere haben eine syntaktische Bedeutung. Alle Zeichen, die Metacharaktere repräsentieren, lassen sich über einen Backslash maskieren und repräsentieren so das eigentliche Zeichen (z.B. `\[`  für die eckige Klammer).

|Zeichen|Metacharakter<br/>Bedeutung innerhalb<br/> einer Zeichenklasse [   ]|Metacharakter<br/> Bedeutung außerhalb<br/> einer Zeichenklasse|
|---|---|---|
|`.`||beliebiges Zeichen|
|`^`|Negierung<br/>`[^a]`: kein `a`|Beginn der Eingabe|
|`$`||Ende der Eingabe|
|`?`||0-1-mal (Quantifizierer `{0,1}`)<br/>`(? ...)`  leitet Direktiven ein|
|`*`||`*`: 0-n-mal (Quantifizier `{0,}`)<br/> `*?`: lazy-Variante|
|`+`||1-n-mal (Quantifizier  `{1,}`)<br/> `+?`: lazy-Variante|
|`|`||oder|
|`[`||Klassenbeginn|
|`]`||Klassenende|
|`{` `}`||von - bis mal (Quantifizierer)|
|`(` `)`||Gruppierung|
|`\`|Maskierung|Maskierung|
|`/`||Begrenzung der RegEx|
|`-`|Bereichskennzeichnung||	

Immer dann, wenn für ein Zeichen in der obigen Tabelle eine Metacharakterbedeutung zugewiesen ist, muss das Zeichen maskiert werden, wenn es als Literal gesucht werden soll.


### Übersicht vordefinierter Zeichenklassen

#### Vordefinierte Zeichenklassen

|RegEx|Erläuterung|
|---|---|
|`.`|Der Punkt passt auf ein beliebiges Zeichen (bei vielen Parsern: bis auf Newline)|
|`\d` = `[0-9]`<br/>`\D`  = `[^0-9]`|digit: Vordefinierte Zeichenklassen für Ziffern. `\d` passt auf alle Ziffern, entspricht also genau `[0-9]`.  (`\D` ist genau das Gegenteil und passt auf alles außer Ziffern.)|
|`\t`, `\n`, `\r`	|Tabulator, linefeed (Zeilenvorschub, LF), carriage return (Wagenrücklauf, CR)<br/>*nix nutzen `\n` als Zeilenumbruch, Windows `\r\n`|
|`\s` = `nicht \S`|whitespace: `\s` ist eine vordefinierte Klasse, die auf Whitespaces passt, also Leerzeichen, Zeilenumbruch, Tabulator usw. `[\t\n\x08\x0c\r]`  (`\S` ist wieder das Gegenteil)|
|`\w` = `[a-zA-Z_0-9]`= `nicht \W`|word: `\w` entspricht `[a-zA-Z_0-9]`, ob Umlaute erkannt werden können, hängt von der Systemkonfiguration ab (`\W` ist die Negation davon)|
|`\b`  = `nicht \B`|Wortgrenze, also Tabulator, Leerzeichen|
|`\xhh`|Zeichen mit Hexadezimalwert hh|
|`\uhhhh`<br/>`\N{EURO SIGN}`|Unicodezeichen mit Hexcode `hhhh` bzw. in Unicode-Notation|

#### Position und Grammatik (außerhalb der Zeichenklassendefinition!)

|RegEx|Erläuterung|
|---|---|
|`^`|	start of string anchor: Anfang der Eingabe. `^ana` passt in `ananas`, aber nicht in `banane`.| 
|`$`|end of string anchor: Ende der Eingabe. `ze$` passt in `Katze`, aber nicht in `Katzen`.| 
|`\A` `\Z`|Stringbeginn und -ende|
|`\<` `\>`|Wortbeginn und -ende|
|`\|`|Oder-Verknüpfung, entweder der Ausdruck links oder der Ausdruck rechts muss passen.|
|`()`|Mit Klammern gruppierte und selektierte _Reguläre Ausdrücke_ können in der Reihenfolge gezielt ausgegeben werden: Ausgabe im RegEx über  `\1` (erste Klammer) `\2` usw… (parserabhängig auch `$1` `$2` usw.) oder per Java über den `matcher.group(1)`|
|`(?<wert>)`|Selektierte Ausdrücke können auch mit einem Namen versehen werden (hier: `wert` ) und über diesen im Matcher (z.B. Java) gezielt angesprochen werden: `(?<wert>[0-9.,]+)\\s*(?<waehrung>[A-Z]{3})`<br/>
`matcher.group("wert")`|
|`(?:)`|Ausdruck wird gruppiert, ist aber nicht selektiv ansprechbar. (geht bei wenigen Parsern)|
|`(?=)`|Ausdruck: "Muss gefolgt werden von" Ausdruck vor der Klammer muss gefolgt werden von Ausdruck in der Klammer. Letzterer wird aber nicht in das Ergebnis geschrieben.|
|`(?!)`|Ausdruck: "Darf nicht gefolgt werden von" (entsprechend wie oben)|

#### POSIX-Zeichenklassen

|RegEx|Erläuterung|
|---|---|
|`\p{Lower}`|	ASCII Kleinbuchstabe: `[a-z]`|
|`\p{Upper}`|	ASCII Großbuchstabe: `[A-Z]`|
|`\p{ASCII}`|	ASCII Zeichen: `[\x00-\x7f]`|
|`\p{Alpha}`|	Buchstabe: `[A-Za-z]`|
|`\p{Digit}`|	Ziffer: `\d`|
|`\p{Alnum}`|	Buchstabe oder Ziffer|


#### Unicode-Zeichenklassen

|RegEx|Erläuterung|
|---|---|
|`\p{Lu}`|	Unicode Großbuchstabe|
|`\p{Ll}`|	Unicode Kleinbuchstabe|
|`\p{Sc}`|	Unicode Währungssymbol|
|`\p{Nl}`|	Unicode Zahl oder Buchstabe|

#### Flags

|RegEx|Erläuterung|
|---|---|
|`(?i)`|Case-insensitive (ASCII)|
|`(?iu)`|Case-insensitive (Unicode)|
|`(?g)`|global match|
|`(?m)`|Multi-line mode|
|`(?s)`|Single-line / dotall mode|

### Beispiele zum Verständnis

- `CSBME` findet exakt die Zeichenkette CSBME
- `^CSBME$` findet eine Zeile, die ausschließlich aus der Zeichenkette CSBME besteht
- `(Hund|Katze|Maus)` findet die Wörter Hund oder Katze oder Maus
- `[Hund|Katze|Maus]` die Buchstaben `HKMadenstuz` sowie die Pipe
- `[0-9]{2}-[0-9]{2}-[0-9]{4}` findet ein Datum der Notation `01-01-2000`
- suche: `([0-9]{2})-([0-9]{2})-([0-9]{4})`<br/>ersetze: `\1\.\2\.\3`<br/>
wandelt ein Datum von der Form 01/01/2000 in die Form 01.01.2000 um.

### RegEx in Programmiersprachen nutzen:

<span class="tabrow" >
   <button class="tablink" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java" style="display:none">

**Text suchen**

Die Klasse String bietet mir der Methode `matches()` die Möglichkeit, unmittelbar zu prüfen, ob ein bestimmtes Muster vorliegt:

```java
if (!text.matches("[0-9]*")) {
  // Findet Werte, die keine (!) Zahlen sind
}
if (text.matches("[0-9A-Za-z]*")) {
  // Findet Zahlen, Buchstaben, ohne Sonderzeichen
}
```

**Teilstrings tauschen**

Man kann aber nicht nur prüfen, ob bestimmte Muster vorkommen, man kann auch konkrete Muster ersetzen (mit der String-Methode `replace()`):

```java
text = "Alle 14 Zahlenziffern, die in diesen 392 Buchstaben an 4 verschiedenen Stellen stehen sollen 1 mal ersetzt werden.";
text = text.replaceAll("[0-9]","X"); 
System.out.println(text);
```

**Komplexere Pattern mit Matches und LookingAt**

Komplexere als die oben genannten Beispiele lassen sich über die Klassen Pattern und Matcher realisieren, die für ein Muster und einen Prüfer stehen. Zunächst die bekannte Darstellung (Zeile „boolean ...“) , darunter dasselbe Problem mit Pattern und Matcher umgesetzt.

```java
String str = "X";
String regex = "[A-Z]"; 

// Test auf Großbuchstabe (ASCII)
boolean matches = str.matches(regex);

// Alternative: pre-compiled pattern
Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(str);
matches = matcher.matches();

// LookingAt
matcher = pattern.matcher("Hallo");
matches = matcher.matches(); // false
startsWith = matcher.lookingAt(); // true
```

**Group**

Die Vorteile von Pattern/Matcher lassen sich insbesondere ausspielen, wenn mehrere Teilbereiche von einer Zeichenkette gesucht sind. Die eingeklammerten Bereiche der RegEx können gezielt (über `matcher.group()`) angesprochen und ausgegeben werden:

```java
String str = "17-25";
String regex = "(\\d+)-(\\d+)"; // Backslash im Sourcecode verdoppeln!

Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(str);
if (matcher.matches()) {
  System.out.printf("Von %s bis %s\n", 
    matcher.group(1), matcher.group(2));
}
```

**Split**

Regular Expressions können auch genutzt werden, um Zeichenketten in (mehrere) Teilzeichenketten zu teilen:

```java
String str = "1,2, 3, 4,   5,6";
String regex = "\\W+";
String[] elements = str.split(regex); // Aufteilen an Wortgrenzen

// Alternative: pre-compiled pattern
Pattern pattern = Pattern.compile(regex);
elements = pattern.split(str);
```

**Find**

Sofern ein Suchmuster öfters gefunden wurden, können alle Ergebnisse mit Hilfe einer while-Schleife durchlaufen werden:

```java
String str = "Alle RDBMS benutzen SQL";
String regex = "\\p{Lu}{2,}";

// Finde all Wörter mit mindestens zwei 
// Großbuchstaben (Unicode)
Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(str);
while (matcher.find()) {
  System.out.println(str.substring(
    matcher.start(), matcher.end()));
}
```

**Replace**

Auch das eingangs erwähnte tauschen von Zeichenketten kann für komplexere Probleme mit Pattern/Matcher umgesetzt werden:

```java
String str = "value  with spaces";
String regex = "\\s+";
String replacement = "+"; // Ersetze whitespace mit +
String no_spaces =  str.replaceAll(regex, replacement);

// Alternative: pre-compiled pattern
Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(str);
no_spaces = matcher.replaceAll(replacement);

// Ersetze ${name} mit System.getProperty(name)
String str = "Hallo ${user.name}!"; 
String regex = "\\$\\{([a-z.]+)\\}";
Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(str);
StringBuffer sb = new StringBuffer();
while (matcher.find()) {
  matcher.appendReplacement(sb, System.getProperty(matcher.group(1)));
}
matcher.appendTail(sb);
System.out.println(sb.toString());
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python">

**Text suchen**

Python bietet mit `re` eine _Library_, die das Handling für _Reguläre Ausdrücke_ komfortablen übernimmt:

```python
import re

text = "Ist hier eine Postleitzahl in acht Wörtern versteckt?"
if (re.search("[0-9]{5}, text)):
   # Text enthält Zahlen
   pass
```

```python
import re

text = "Ist hier eine Postleitzahl in acht Wörtern versteckt?"
ergebnisliste = re.findall("[acehinr]+", text)
print(ergebnisliste)
```

**Text aufteilen**

```python
import re

text = "Ist hier eine Zahl in acht Wörtern versteckt?"
ergebnisliste = re.split("[in]+", text)
print(ergebnisliste)
```

**Text ersetzen**

```python
import re

text = "Drei Flötisten mit dem Kontrabass."
ergebnis = re.sub("[aeiouAEIOUÄÖÜäöü]+", "u", text)
print(ergebnis)
```

**Teile eines Suchmusters ersetzen**

```python
import re

text = "030kleinbuchstabenersetzenDASHIERNICHT."
ergebnis = re.sub("([0-9]*)([^A-Z]*)([A-Z]*)", r'\g<1> neuer Text \g<3> \g<2>', text)

print(ergebnis)
```

Die im ersten String mit Klammern gruppierten Gruppenergebnisse können über `\g<1>` bis `\g<n>` der Reihe nach referenziert werden. `\g<0>` referenziert den gesamten gefundenen Ausdruck. Das führende `r` bei `r'\g<1>...'` setzt den String in den _raw_-Mode, auf diese Art müssen Schrägstriche nicht maskiert werden. 

Mithilfe dieser Funktion können komplexe Suchen/Ersetzen-Aufgaben erledigt werden. Obiges Programm/Pattern gibt beispielsweise `030 neuer Text DASHIERNICHT kleinbuchstabenersetzen` aus.

</span>

### RegEx in Powershell und Bash

#### Mit RegEx über das Terminal nach Dateiinhalten suchen

Beispiel: in einer Datei befindet sich irgendwo ein ungewöhnliches Zeichen, weswegen die Datei nicht verarbeitet werden kann. Folgende Aufrufe mit regulären Ausdrücken finden die betreffenden Zeilen:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="shell" data-tabid="powershell" onclick="openTabsByDataAttr('powershell', 'shell')">PowerShell</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="bash" onclick="openTabsByDataAttr('bash', 'shell')">Linux Terminal / Bash</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="all" onclick="openTabsByDataAttr('all', 'shell')">_all_</button>
    <button class="tablink" data-tabgroup="shell" data-tabid="none" onclick="openTabsByDataAttr('none', 'shell')">_none_</button>
</span>

<span class="tabs" data-tabgroup="shell" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="powershell">

```powershell
get-childitem *.md | select-string -pattern [^!-~\s\w] -allmatches -CaseSensitive`
```

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="bash">

```bash
grep -onP "[^\x00-\x7FöüäßÜÜ]" *.md
```

</span>

#### Mit RegEx über das Terminal Dateiinhalte verändern

<span class="tabs" data-tabgroup="shell" data-tabid="bash">

In einer Textdatei soll bei allen Geldbeträgen, die mit `EUR` enden das Zeichen € gesetzt werden. Da im Text auch an anderen Stellen `EUR` steht, muss sichergestellt werden, dass nur bei den Geldbeträgen die Währungsbezeichnung ersetzt wird. Folgender Aufruf ändert das direkt in der Datei:
 
```bash
sed -E -i "s/([0-9\.]+,[0-9]{2})(\ ?EUR)\)/\1 €)/g" dateiname.txt
```

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="powershell">

Noch kein Beispiel für die Powershell vorhanden

</span>

### RegEx in SQL-Abfragen


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="Oracle" onclick="openTabsByDataAttr('oracle', 'sql')">Oracle</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="sqlite" onclick="openTabsByDataAttr('sqlite', 'sql')">SQLite</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

MariaDB bietet eine ganze Reihe von Funktionen zur Auswertung von Regulären Ausdrücken an. Eine Übersicht findet sich [unter diesem Link](https://mariadb.com/kb/en/regular-expressions-functions/)

Beispielsweise findet man alle Namen, die mit `H` anfangen und ein Doppelname sind (also mit Bindestrich) über folgende Abfrage:

```sql
SELECT
firstname 
FROM User 
WHERE firstname REGEXP "^H.*-.*"
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

SQL-Server bietet keine native Unterstützung für RegEx. Allerdings liefert `PATINDEX` die Möglichkeit Muster zu beschreiben, die sehr nah an Reguläre Ausdrücke herankommen. Weitere Infos [z.B. unter diesem Link.](https://learn.microsoft.com/de-de/sql/t-sql/functions/patindex-transact-sql)


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

PostgreSQL bietet mit den Funktionen `regexp_instr()` und `regexp_like()` Möglichkeiten, die Mustererkennung komfortabel durchzuführen. Weitere Infos dazu [unter diesem Link.](https://www.postgresql.org/docs/current/functions-matching.html)

```sql
SELECT * FROM db.table WHERE regexp_like( text, "[A-Z]")
```

```sql
SELECT regexp_replace ( text, '[aeiou]', 'u') FROM db.table 
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="oracle" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%Oracle%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="sqlite" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%SQLite%'
```

</span>


### Links und weitere Informationen

- [Gute RegEx-Übersicht und Java-Code zur Nutzung von Martin Kompf](https://www.kompf.de/java/regex.html)

- [Learn Regular expressions in 55 Minutes](https://qntm.org/files/re/re.html)

- [RegEx101 - RegEx online ausprobieren](https://regex101.com/)

- [RegExR - RegEx online ausprobieren](https://regexr.com/)

