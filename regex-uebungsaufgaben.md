## Übungsaufgaben zu Regular Expressions

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/regex-uebungsaufgaben</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110397061085890869</span>


> **tl/dr;** _(ca. 20 min Lesezeit): Reguläre Ausdrücke gelten als nicht wartbar. Zu kryptisch ist ihr Aufbau. Trotzdem kann man mit etwas Übung einen Blick dafür entwickeln, warum Zeichenketten durch Muster gefunden werden oder nicht gefunden werden. Hier sind ein paar Übungsaufgaben, um den Blick zu schärfen!_

### Übungsaufgabe KFZ-Kennzeichen

Zur Validierung zu erfassender KFZ-Kennzeichen soll ein regulärer Ausdruck verwendet werden. Es sollen folgende Regeln gelten (die nicht den tatsächlichen KFZ-Regeln entsprechen):

- Die Kennzeichen setzten sich aus 3 Elementen getrennt durch 2 Trennzeichen zusammen:
Ortsziffer Bindestrich oder Freizeichen Großbuchstaben Freizeichen Zahl

-	Bei Kennzeichen mit einstelliger Ortziffer (z.B. Berlin: B) folgen die Elemente Großbuchstaben und Zahl mit zusammen bis zu sieben Zeichen, 

-	bei Kennzeichen mit zweistelliger Ortziffer (z.B. Bielefeld: BI) folgen die Elemente Großbuchstaben und Zahl mit zusammen bis zu sechs Zeichen,

-	bei Kennzeichen mit dreistelliger Ortziffer (z.B. Lippe: LIP) folgen die Elemente Großbuchstaben und Zahl mit zusammen bis zu fünf Zeichen

-	mindestens das erste und höchstens die ersten beiden Zeichen der mittleren Gruppe sind Großbuchstaben 

-	höchstens bis zur genannten Anzahl an Gesamtzeichen wird mit Zahlen aufgefüllt

Hinweis: `\w` = `[a-zA-Z_0-9]`   `\d` = `[0-9]`  `\s` = Whitespace (Leerzeichen, Zeilenumbruch, Tabulator), in den letzten beiden Spalten wurde das Leerzeichen grau hinterlegt, damit es nicht übersehen wird (`\ `)

a) Welche Testfälle werden durch die drei RegEx jeweils gefunden (in der Spalte kreuzen (x) = wird durch die RegEx vollständig gefunden / Strich (-) = wird nicht gefunden) (21 Punkte)


|Testfälle|Akzeptanz<br/>-kriterium|`[^a-z]{3}\-[^a-z]{2}\s\d*`|`[\w]+\-[\w]+\ \d+`|`[A-Z]+(-|\s)[\w\ ]{1,8}`|
|---|---|---|---|---|
|LIP-PE 123|X|
|lip-pe 123|-|		
|BI ER 9876|X|			
|B-U 56789|X|		
|PB-Z 11|X|		
|HF-ABC 654|-|	
|MI-LA 54321|-|		

<button onclick="toggleAnswer('regex1a')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="regex1a">

|Testfälle|Akzeptanz<br/>-kriterium|`[^a-z]{3}\-[^a-z]{2}\s\d*`|`[\w]+\-[\w]+\ \d+`|`[A-Z]+(-|\s)[\w\ ]{1,8}`|
|---|---|---|---|---|
|LIP-PE 123|X|X|X|X|
|lip-pe 123|-|-|X|-|
|BI ER 9876|X|-|-|X|
|B-U 56789|X|-|X|X|
|PB-Z 11|X|-|X|X|
|HF-ABC 654|-|-|X|X|	
|MI-LA 54321|-|-|X|X|

</span>

b) Erstellen Sie einen regulären Ausdruck, der die oben genannten Akzeptanzkriterien (Kreuz) erfüllt und negierte Einträge (Strich) nicht findet. Der reguläre Ausdruck soll selbstverständlich generisch sein und nicht einfach eine oder-verknüpfte Liste der gesuchten Zeichenketten! (7 Punkte)

<button onclick="toggleAnswer('regex1b')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="regex1b">

Zum Beispiel:

```regex
[A-Z][A-Z\-\ ]{3}[A-Z\ \-\d]{3}\d{1,3}
```

</span>

### Übungsaufgabe ISBN

Die Erfassung der Identifikationsnummern für Bücher (ISBN) kann in unterschiedlichen Formaten geschehen. Im Fall der ISBN-13 wäre das vier Gruppen, die jeweils durch einen Bindestrich getrennt werden:

- 3-stelliger Präfix

- Gruppennummer (1 oder mehrstellig)

- Verlagsnummer (1 oder mehrstellig)

- Titelnummer (1 oder mehrstellig)

- Prüfziffer (1-stellig)

Neben der ISBN-13 ist noch die ISBN-10 verbreitet, in der die Gruppe "Präfix" fehlt. Darüber hinaus gibt es die Darstellung als internationale Artikelnummer (EAN), die einer ISBN-13 ohne Bindestriche entspricht. Als Akzeptanzkriterium wird erwartet, dass alle als gültig markierten Testfälle ("X") bestanden werden und die weiteren scheitern. Sämtliche Zahlen in den Testfällen müssen natürlich beliebig durch andere Zahlen ersetzbar sein.

Hinweis: `\w` = `[a-zA-Z_0-9]`   `\d` = `[0-9]`  `\s` = Whitespace (Leerzeichen, Zeilenumbruch, Tabulator), in den letzten beiden Spalten wurde das Leerzeichen grau hinterlegt, damit es nicht übersehen wird (`\ `)


a) Welche Testfälle werden durch die drei RegEx jeweils gefunden (in der Spalte Kreuzen = wird durch die RegEx vollständig gefunden wird – Strich = wird nicht gefunden) 
(10,5 Punkte)

|Testfälle|Akzeptanz<br/>-kriterium|`(ISBN )?[^A-Z^\s]{13}`|`[A-Z]{1,4}\s+[0-9]*`|`(ISBN\|EAN).[\d\-]{17}`|
|---|---|---|---|---|
|ISBN 978-3-86680-192-9|X|			
|ISBN 9783866801929|X|
|ISBN 3-86680-192-9|X|	
|EAN 9783866801929|X|		
|ISBN 97838668019|-|		
|ISBN 978 3 86680 192 9|-|			
|3-86680-192-9|-|	
	
<button onclick="toggleAnswer('regex2a')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="regex2a">

|Testfälle|Akzeptanz<br/>-kriterium|`(ISBN )?[^A-Z^\s]{13}`|`[A-Z]{1,4}\s+[0-9]*`|`(ISBN\|EAN).[\d\-]{17}`|
|---|---|---|---|---|
|ISBN 978-3-86680-192-9|X|-|-|X|
|ISBN 9783866801929|X|X|X|-|
|ISBN 3-86680-192-9|X|X|-|-|
|EAN 9783866801929|X|-|X|-|
|ISBN 97838668019|-|-|X|-|
|ISBN 978 3 86680 192 9|-|-|-|-|
|3-86680-192-9|-|X|-|-|

</span>

b) Erstelle einen Regulären Ausdruck, die oben genannten Akzeptanzkriterien (Kreuz in Spalte) erfüllt.

<button onclick="toggleAnswer('regex2b')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="regex2b">

Beispielsweise:
```regex
(ISBN|EAN)\ [\d\-]{13,17}
```

</span>

### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)

- [Gute RegEx-Übersicht und Java-Code zur Nutzung von Martin Kompf](https://www.kompf.de/java/regex.html)

- [Learn Regular expressions in 55 Minutes](https://qntm.org/files/re/re.html)

- [RegEx101 - RegEx online ausprobieren](https://regex101.com/)

- [RegExR - RegEx online ausprobieren](https://regexr.com/)

